name := "ETF-Steuer"

version := "1.4-SNAPSHOT"

scalaVersion := "3.1.2"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.11" % "test"

// https://mvnrepository.com/artifact/com.typesafe/config
libraryDependencies += "com.typesafe" % "config" % "1.4.2"