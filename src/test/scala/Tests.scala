import com.typesafe.config.{Config, ConfigFactory}
import org.scalatest.funsuite.AnyFunSuite
import treufuss.etfsteuer._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Random

class Tests extends AnyFunSuite {

  val config: Config = ConfigFactory.load()

  given Parameters = Parameters.fromConfig(config)

  test("FractionalQueue") {
    case class Element(d: Double, s: String, creation: Long) extends FractionalQueueElement[Element] {
      override def get: Element = this

      override def fractionalValue: Double = d

      override def withFractionalValue(arg: Double): FractionalQueueElement[Element] = Element(arg, String.format("%.2f", arg), creation)
    }

    val random = new Random(0L)

    val list = Range(0, 5).map(_ => random.nextDouble() * 100).map(d => Element(d, String.format("%.2f", d), System.currentTimeMillis()))

    val queue = FractionalQueue(list)

    println(queue.mkString("\n"))

    val total = queue.total
    println(s"total: $total")

    val q1 = queue.dequeue(queue.head.fractionalValue / 2)
    println("Remove from first")
    println(q1.mkString("\n"))

    val q2 = queue.dequeue(total / 2)
    println("Remove half")
    println(q2.mkString("\n"))

    val q2b = q2.enqueue(Element(1.0, "1.00", System.currentTimeMillis()))
    println("Enqueue")
    println(q2b.mkString("\n"))
    println("Difference: " + (q2b.total - q2.total))

    val q3 = queue.dequeue(total)
    println(s"Empty")
    println(q3.mkString("\n"))

    val q4 = queue.dequeue(total + FractionalQueue.MinPrecision / 2)
    println(s"Tolerance Fine")
    println(q4.mkString("\n"))

    println(s"Tolerance Fail")
    assertThrows[IndexOutOfBoundsException](queue.dequeue(total + FractionalQueue.MinPrecision))

    val peek = queue.peek(100.0)
    println("Peek 100:")
    println(peek.mkString("\n"))
    assert(peek.map(_.fractionalValue).sum - 100.0 < 0.0001)

    assert(queue.peek(0.0).isEmpty)
    assertThrows[IndexOutOfBoundsException](queue.peek(total + FractionalQueue.MinPrecision))
  }

  ignore("FractionalQueue Benchmark") {
    val random = new Random(0L)

    val queue = FractionalQueue.empty[Purchase]

    val start = System.currentTimeMillis()
    for (_ <- 1 to 1000000000) {
      if (queue.total > 100.0 && random.nextBoolean())
        queue.dequeue(random.nextDouble() * queue.total)
      else
        queue.enqueue(Purchase(random.nextDouble() * 10, 100.0))
    }
    val end = System.currentTimeMillis()
    println(String.format("benchmark result: %.3f seconds", (end - start) / 1000.0))
  }

  test("FIFO") {
    val position: Position = Position(
      "TEST",
      "ETF123",
      15.0,
      FractionalQueue(List(
        Purchase(10, 100),
        Purchase(10, 140)
      )))

    println(position)
    val updated = position.sell(15, 15)
    println(updated)
    assert(math.abs(updated.totalNumber - 5.0) < 0.000001)
    assert(math.abs(updated.totalGrossWorth - 75.0) < 0.000001)
    assert(math.abs(updated.recentGrossGain - 55.0) < 0.000001)
  }

  test("best case") {

    val position = Position(
      "Best Case ETF",
      "ETF001",
      100,
      FractionalQueue(List(Purchase(100, 0)))
    )

    val depot = Depot(Map("ETF001" -> position))

    println(depot.sell("ETF001", 100, 100).positions.head._2.recentGrossGain)

    depot.execute
  }

  test("returns") {

    val positions = for (gain <- 0 to 50 by 2) yield String.format("ETF%03d", gain) -> Position(
      s"ETF with $gain% total return",
      String.format("ETF%03d", gain),
      100 + gain,
      FractionalQueue(List(Purchase(500, 500 * 100)))
    )

    val depot = Depot(positions.toMap)

    depot.execute
  }

  test("shares available") {

    val positions = for (available <- 1 to 25 by 1) yield String.format("ETF%03d", available) -> Position(
      s"50%+ ETF with $available shares available",
      String.format("ETF%03d", available),
      150,
      FractionalQueue(List(Purchase(available, available * 100)))
    )

    val depot = Depot(positions.toMap)

    depot.execute
  }

  test("scenarios") {
    var index = 0

    val positions = for ((name, num, buyPrice, currPrice) <- List(
      ("Theoretical Best Case", 100, 0, 100),
      ("  0% total return", 250, 100, 100),
      ("  2% total return", 250, 100, 102),
      ("  5% total return", 250, 100, 105),
      (" 10% total return", 250, 100, 110),
      (" 15% total return", 250, 100, 115),
      (" 20% total return", 250, 100, 120),
      (" 50% total return", 250, 100, 150),
      (" 75% total return", 250, 100, 175),
      ("100% total return", 250, 100, 200),
      ("200% total return", 250, 100, 300),
      (" 50% total return,  1 share available", 1, 100, 150),
      (" 50% total return,  2 share available", 2, 100, 150),
      (" 50% total return,  5 share available", 5, 100, 150),
      (" 50% total return, 10 share available", 10, 100, 150),
      (" 50% total return, 15 share available", 15, 100, 150),
      (" 50% total return, 20 share available", 20, 100, 150),
    )) yield {
      index = index + 1
      String.format("%05d", index) -> Position(
        name,
        String.format("%05d", index),
        currPrice,
        FractionalQueue(List(Purchase(num, num * buyPrice)).reverse)
      )
    }

    val depot = Depot(positions.toMap)

    depot.execute
  }

  test("run main function") {
    Main.main(Array.empty)
  }

}
