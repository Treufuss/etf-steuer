import treufuss.etfsteuer.Depot

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

object Main extends App {

  Depot.load() match {
    case Failure(exception) => exception.printStackTrace()
    case Success(depot) =>
      println("Optimum")
      depot.execute
      println()
      println("Sell Out")
      depot.sellOut()
  }
}
