package treufuss.etfsteuer

import com.typesafe.config.ConfigFactory

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import scala.collection.immutable.ListMap
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.io.Source
import scala.jdk.CollectionConverters.IteratorHasAsScala
import scala.language.reflectiveCalls
import scala.math.BigDecimal.double2bigDecimal
import scala.util.{Try, Using}

case class Depot(positions: Map[String, Position])(using parameters: Parameters) {

  import parameters._

  def buy(isin: String, number: Double, price: Double): Depot = {
    this.copy(positions = positions.updated(isin, positions(isin).buy(number, price)))
  }

  def sell(isin: String, number: Double, pricePerShare: Double): Depot = {
    this.copy(positions = positions.updated(isin, positions(isin).sell(number, pricePerShare)))
  }

  private def singleResult(position: Position, num: Double) = {
    val currFreibetragFactor = if (freibetragExceptions.contains(position.isin)) 1.0 else freibetragFactor

    val pos = position.sell(num)
    val sellWorth = num * position.currentPricePerShare
    val sellGain = pos.recentGrossGain
    val taxRaw = math.max(sellGain * currFreibetragFactor * taxRate, 0.0)
    val sellFee = pos.recentFee
    val taxNow = math.max((sellGain - sellFee) * currFreibetragFactor - pausch, 0.0) * taxRate
    val pauschRemain = math.min(math.max(pausch - (sellGain - sellFee) * currFreibetragFactor, 0.0), pausch)
    val pauschUsed = pausch - pauschRemain
    val buyFee = feeCalculator.byNetTotal(math.max(sellWorth - sellFee - taxNow, 0.0))

    val missedGains = (sellFee + taxNow + buyFee) * math.pow(parameters.rateOfReturn, parameters.years) - (sellFee + taxNow + buyFee)
    val saving = taxRaw - missedGains - (sellFee + taxNow + buyFee)
    val spread = if (sellWorth > 0.0) String.format("%.2f %%", (saving / sellWorth) * 100) else ""
    saving -> ListMap(
      "isin" -> position.isin,
      "name" -> position.name,
      "price" -> position.currentPricePerShare,
      "shares" -> position.totalNumber,
      "sellNum" -> num,
      "sellWorth" -> sellWorth,
      "sellGain" -> sellGain,
      "taxRaw" -> taxRaw,
      "taxNow" -> taxNow,
      "sellFee" -> sellFee,
      "buyFee" -> buyFee,
      "missedGains" -> missedGains,
      "saving" -> saving,
      "spread" -> spread,
      "pauschUsed" -> pauschUsed,
      "pauschRemain" -> pauschRemain
    )
  }

  private def linearSearch(position: Position, left: Double, right: Double, stepSize: Double) = {
    assert(left >= 0)
    assert(right <= position.totalNumber)

    val resultList = for (num <- left to right by stepSize) yield {
      singleResult(position, num.toDouble)._2
    }

    resultList.maxBy(_.apply("saving").asInstanceOf[Double])
  }

  def execute(using ec: ExecutionContext): Unit = {
    val resultsFut = for ((_, position) <- positions.withFilter(_._2.totalNumber >= 1.0)) yield Future {

      val quickResult = linearSearch(position, 0.0, position.totalNumber, 1.0)

      val (left, right) = (
        math.max(quickResult("sellNum").asInstanceOf[Double] - 1.0, 0.0),
        math.min(quickResult("sellNum").asInstanceOf[Double] + 1.0, position.totalNumber)
      )

      if (tradeFractions) linearSearch(position, left, right, stepSize = 0.00001)
      else quickResult
    }

    val results = Await.result(Future.sequence(resultsFut), Duration.Inf).toList

    println(Util.prettyTable(results.sortBy(-_.apply("saving").asInstanceOf[Double])))
  }

  def sellOut(): Unit = {
    val results = for ((_, position) <- positions.withFilter(_._2.totalNumber >= 1.0)) yield {
      linearSearch(position, position.totalNumber, position.totalNumber, 1.0)
    }

    println(Util.prettyTable(results.toSeq.sortBy(-_.apply("saving").asInstanceOf[Double])))
  }

  def executeSingle(isin: String, sell: Double): Unit = {
    assert(sell >= 0, "Cannot sell negative number")
    assert(positions.isDefinedAt(isin), s"$isin does not exist")

    val num = math.min(sell, positions(isin).totalNumber)

    val result = linearSearch(positions(isin), num, num, 1.0)

    println(Util.prettyTable(List(result)))
  }
}

object Depot {
  def load(): Try[Depot] = {
    val config = ConfigFactory.load()

    given parameters: Parameters = Parameters.fromConfig(config)
    import Util.parseDouble
    import parameters.csvParameters

    val pathToPriceSource = config.getString("csv.paths.prices")
    val pathToTransactionsSource = config.getString("csv.paths.transactions")

    val currentPrices = pathToPriceSource.toLowerCase match {
      case "yahoo" => Util.pricesFromYahoo(pathToTransactionsSource)
      case path => Util.pricesFromTable(path)
    }

    Using(Source.fromFile(pathToTransactionsSource, csvParameters.encoding))(_.getLines().toList.map(_.split(';').map(_.trim))) map {
      case Nil => throw new Exception("No Transactions Found in CSV")
      case headers :: rows =>
        val (indexOfDate, indexOfPrice, indexOfNumber, indexOfIsin, indexOfName) = (
        headers.indexOf(csvParameters.transactionDateCol),
        headers.indexOf(csvParameters.transactionTotalPriceCol),
        headers.indexOf(csvParameters.transactionNumberCol),
        headers.indexOf(csvParameters.transactionIsinCol),
        headers.indexOf(csvParameters.transactionNameCol),
        )

        val formatter = DateTimeFormatter.ofPattern(csvParameters.datePattern)

        val typedRows = rows.map(row =>
          (
            LocalDateTime.parse(row(indexOfDate), formatter),
            row(indexOfPrice).parseDouble,
            row(indexOfNumber).parseDouble,
            row(indexOfIsin),
            row(indexOfName)
          )
        ).sortBy(_._1)

        val positions = for ((isin, group) <- rows.groupBy(arr => arr(indexOfIsin))) yield {
          isin -> Position(group.head(indexOfName), isin, currentPrices(isin))
        }

        var depot = Depot(positions)

        for ((date, price, number, isin, name) <- typedRows) {
          val absNumber = math.abs(number)
          val absPrice = math.abs(price)
          if (price > 0 && number > 0) {
            depot = depot.buy(isin, absNumber, absPrice)
          } else {
            depot = depot.sell(isin, absNumber, absPrice / absNumber)
          }
        }

        depot
    }
  }
}
