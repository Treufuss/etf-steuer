package treufuss.etfsteuer

import scala.io.Source
import scala.util.{Failure, Success, Using}

object Util {

  extension (s: String) {
    def parseDouble(using parameters: Parameters): Double = parameters.csvParameters.decimalSeparator match {
      case "," =>
        s.replaceAll("\\.", "")
          .replaceAll(",", ".").toDouble
      case "." =>
        s.replaceAll(",", "").toDouble
      case sep => throw new Exception("Decimal separator must be either '.' or ',': " + sep)
    }
  }

  def prettyTable(map: Seq[Map[String, Any]]): String = {
    val headers = map.head.keys.toList
    val values = map.map(row => headers.map(row(_)))
    prettyTable(headers, values)
  }

  def prettyTable(headers: Seq[String], values: Seq[Seq[Any]]): String = {
    val stringValues = values map { row =>
      headers.zip(row) map {
        case (key, value: Int) => value.toString
        case (key, value: Double) if key == "sellNum" => String.format("%.5f", value)
        case (key, value: Double) if key == "price" => String.format("%.3f", value)
        case (key, value: Double) => String.format("%.2f", value)
        case (key, value) => value.toString
      }
    }

    val maxLengths = (headers.map(_.length) +: stringValues
      .map(_.map(_.length)))
      .reduce((l, r) => l.zip(r).map(t => math.max(t._1, t._2)))

    val paddedHeaders = headers
      .zip(maxLengths)
      .map({ case (header, length) => header.padTo(length, ' ') })

    val separators = maxLengths.map(length => "-" * length)

    val paddedValues = stringValues.map(_.zip(maxLengths).map({
      case (value, length) if value.matches("""-?\d+(,\d+)?( \%)?""") => String.format(s"%${length}s", value)
      case (value, length) => String.format(s"%-${length}s", value)
    }))

    val lines = (paddedHeaders +: separators +: paddedValues) map { row =>
      row.map(" " + _ + " ").mkString("|", "|", "|")
    }

    lines.mkString("\n")
  }

  def pricesFromTable(pathToPriceSource: String)(using parameters: Parameters): Map[String, Double] = {
    import parameters.csvParameters

    Using(Source.fromFile(pathToPriceSource, csvParameters.encoding))(
      _.getLines().toList.map(_.split(csvParameters.separator).map(_.trim))) match {
      case Failure(exception) =>
        exception.printStackTrace()
        Map.empty[String, Double].withDefaultValue(0.0)
      case Success(headers :: rows) =>
        rows
          .map(row => row(headers.indexOf(csvParameters.currentIsinCol)) -> row(headers.indexOf(csvParameters.currentPriceCol)).parseDouble)
          .toMap.withDefaultValue(0.0)
      case Success(Nil) => Map.empty[String, Double].withDefaultValue(0.0)
    }
  }

  def pricesFromYahoo(pathToTransactionsSource: String)(using parameters: Parameters): Map[String, Double] = {
    import parameters.csvParameters
    Using(Source.fromFile(pathToTransactionsSource, csvParameters.encoding))(
      _.getLines().map(_.split(csvParameters.separator).map(_.trim)).toList) match {
      case Failure(exception) =>
        exception.printStackTrace()
        Map.empty[String, Double].withDefaultValue(0.0)
      case Success(headers :: rows) if csvParameters.transactionTickerCol.isDefined =>
        rows
          .map(
            row => row(headers.indexOf(csvParameters.transactionIsinCol)) -> row(headers.indexOf(csvParameters.transactionTickerCol.get))
          ).distinct.map({
          case (isin, ticker) =>
            val extractor = """.*"regularMarketPrice":\{"raw":([^,]*),.*""".r

            val price = Using(Source.fromURL(s"https://query1.finance.yahoo.com/v10/finance/quoteSummary/$ticker?modules=price"))(
              _.getLines().mkString match {
                case extractor(value) => value.toDouble
                case s => throw new Exception("fuck: " + s)
              }
            ).getOrElse(0.0)

            (isin, price)
        }).toMap
      case _ => Map.empty[String, Double].withDefaultValue(0.0)
    }
  }
}
