package treufuss.etfsteuer

case class FeeCalculator(feePercentage: Double, feeFix: Double, feeLimit: Double) {

  def byNumberOfShares(numberOfShares: Double, pricePerShare: Double): Double = {
    assert(pricePerShare >= 0.0  && numberOfShares >= 0.0, "input parameters must not be negative")
    val grossTotal = numberOfShares * pricePerShare
    if (numberOfShares > 0.0)
      math.min(grossTotal * feePercentage + feeFix, feeLimit)
    else 0.0
  }

  /**
   * Note that the fee is not simply
   * netTotal * feePercentage + feeFix
   *
   * Because then the buy transaction would cost more than the netTotal
   *
   * Instead the buyFee must be part of the netTotal such that
   * (1) netTotal = x + buyFee
   * (2) buyFee = x * feePercentage + feeFix
   */
  def byNetTotal(netTotal: Double): Double = {
    assert(netTotal >= 0.0, "netTotal must not be negative")
    if (netTotal > 0.0) {
      val fee = (feePercentage * netTotal + feeFix) / (1 + feePercentage)
      math.min(math.max(fee, feeFix), feeLimit)
    }
    else 0.0
  }

}
