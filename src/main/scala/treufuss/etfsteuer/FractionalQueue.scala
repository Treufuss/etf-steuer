package treufuss.etfsteuer

import treufuss.etfsteuer.FractionalQueue.MinPrecision

import scala.collection.mutable.ListBuffer

case class FractionalQueue[A](private val _elements: Vector[FractionalQueueElement[A]]) extends Seq[A] {

  lazy val total: Double = _elements.map(_.fractionalValue).sum

  def enqueue(element: FractionalQueueElement[A]): FractionalQueue[A] = FractionalQueue(_elements :+ element)

  def dequeue(amount: Double): FractionalQueue[A] = {
    var remain = amount
    val buffer = _elements.toBuffer
    while (remain > 0.0) {
      buffer.headOption match {
        case Some(value) if value.fractionalValue > remain =>
          buffer.update(0, value.withFractionalValue(value.fractionalValue - remain))
          remain = 0.0
        case Some(value) =>
          buffer.dropInPlace(1)
          remain = remain - value.fractionalValue
        case None if remain < MinPrecision =>
          remain = 0.0
        case None =>
          throw new IndexOutOfBoundsException(s"$amount > $total")
      }
    }
    FractionalQueue(buffer.toVector)
  }

  def peek(amount: Double): Seq[A] = {
    var remain = amount
    val buffer = ListBuffer.empty[A]
    for (elem <- _elements) {
      if (remain > 0.0) {
        if (elem.fractionalValue > remain) {
          buffer += elem.withFractionalValue(remain).get
          remain = 0.0
        } else {
          buffer += elem.get
          remain = remain - elem.fractionalValue
        }
      }
    }
    if (remain < MinPrecision)
      buffer.toSeq
    else throw new IndexOutOfBoundsException(s"$amount > $total")
  }

  override def apply(i: Int): A = _elements(i).get

  override def length: Int = _elements.length

  override def iterator: Iterator[A] = _elements.map(_.get).iterator
}

object FractionalQueue {

  val MinPrecision: Double = 1e-10

  def apply[A](values: Seq[FractionalQueueElement[A]]): FractionalQueue[A] =
    new FractionalQueue(values.toVector)

  def apply[A](values: Seq[A])(using toFractionalQueueElement: A => FractionalQueueElement[A]): FractionalQueue[A] =
    new FractionalQueue(values.map(toFractionalQueueElement).toVector)

  def empty[A]: FractionalQueue[A] = FractionalQueue(Vector.empty)
}

trait FractionalQueueElement[A] {
  def get: A

  def fractionalValue: Double

  def withFractionalValue(arg: Double): FractionalQueueElement[A]
}