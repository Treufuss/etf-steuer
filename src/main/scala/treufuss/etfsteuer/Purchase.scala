package treufuss.etfsteuer

/**
 * @param shares        number of stocks traded
 * @param netTotalPrice total price of the transaction including fees
 */
case class Purchase(
                     shares: Double,
                     netTotalPrice: Double
                   ) extends FractionalQueueElement[Purchase] {
  override def get: Purchase = this

  override def fractionalValue: Double = shares

  override def withFractionalValue(arg: Double): FractionalQueueElement[Purchase] = {
    assert(arg >= 0.0, "value must be positive")
    val newPrice = netTotalPrice * arg / shares
    Purchase(arg, newPrice)
  }
}
