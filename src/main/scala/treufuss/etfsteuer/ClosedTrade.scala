package treufuss.etfsteuer

case class ClosedTrade(
                        shares: Double,
                        pricePerShare: Double,
                        grossGain: Double,
                        fee: Double
                      )
