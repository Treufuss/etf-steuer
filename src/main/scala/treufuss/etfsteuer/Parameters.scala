package treufuss.etfsteuer

import com.typesafe.config.Config

import scala.jdk.CollectionConverters.CollectionHasAsScala
import scala.util.Try

case class Parameters(
                       pausch: Double = 801.0,
                       taxRate: Double = 0.2675,
                       freibetragFactor: Double = 0.7,
                       freibetragExceptions: List[String] = List("US88160R1014"),
                       feePercentage: Double = 0.0025,
                       feeFix: Double = 4.9,
                       feeLimit: Double = 69.9,
                       rateOfReturn: Double = 1.07,
                       years: Int = 10,
                       tradeFractions: Boolean = true,
                       csvParameters: CsvParameters
                     ) {
  val feeCalculator: FeeCalculator = FeeCalculator(feePercentage, feeFix, feeLimit)
}

object Parameters {
  def fromConfig(config: Config): Parameters = Parameters(
    pausch = config.getDouble("parameters.pausch"),
    taxRate = config.getDouble("parameters.taxRate"),
    freibetragFactor = config.getDouble("parameters.freibetrag"),
    freibetragExceptions = config.getStringList("parameters.freibetragExceptions").asScala.toList,
    feePercentage = config.getDouble("parameters.feePercentage"),
    feeFix = config.getDouble("parameters.feeFix"),
    feeLimit = config.getDouble("parameters.feeLimit"),
    rateOfReturn = config.getDouble("parameters.rateOfReturn"),
    years = config.getInt("parameters.years"),
    tradeFractions = config.getBoolean("parameters.tradeFractions"),
    csvParameters = CsvParameters.fromConfig(config)
  )
}

case class CsvParameters(
                          separator: String,
                          decimalSeparator: String,
                          encoding: String,
                          datePattern: String,
                          transactionDateCol: String,
                          transactionTotalPriceCol: String,
                          transactionNumberCol: String,
                          transactionIsinCol: String,
                          transactionNameCol: String,
                          transactionTickerCol: Option[String],
                          currentIsinCol: String,
                          currentPriceCol: String,
                        )

object CsvParameters {
  def fromConfig(config: Config): CsvParameters = CsvParameters(
    separator = config.getString("csv.separator"),
    decimalSeparator = config.getString("csv.decimalSeparator"),
    encoding = config.getString("csv.encoding"),
    datePattern = config.getString("csv.datePattern"),

    transactionDateCol = config.getString("csv.transactionsColumns.date"),
    transactionTotalPriceCol = config.getString("csv.transactionsColumns.price"),
    transactionNumberCol = config.getString("csv.transactionsColumns.number"),
    transactionIsinCol = config.getString("csv.transactionsColumns.isin"),
    transactionNameCol = config.getString("csv.transactionsColumns.name"),
    transactionTickerCol = Try(config.getString("csv.transactionsColumns.ticker")).toOption,

    currentIsinCol = config.getString("csv.currentPricesColumns.isin"),
    currentPriceCol = config.getString("csv.currentPricesColumns.price")
  )
}
