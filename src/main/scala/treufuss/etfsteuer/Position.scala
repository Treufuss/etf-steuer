package treufuss.etfsteuer

case class Position(
                     name: String,
                     isin: String,
                     currentPricePerShare: Double,
                     openTrades: FractionalQueue[Purchase] = FractionalQueue.empty,
                     closedTrades: List[ClosedTrade] = List.empty
                   )(using parameters: Parameters) {

  lazy val recentGrossGain: Double = closedTrades.headOption.map(_.grossGain).getOrElse(0.0)

  lazy val recentFee: Double = closedTrades.headOption.map(_.fee).getOrElse(0.0)

  lazy val totalInvest: Double = openTrades.map(_.netTotalPrice).sum

  lazy val totalNumber: Double = openTrades.map(_.shares).sum

  lazy val totalGrossWorth: Double = totalNumber * currentPricePerShare

  def buy(number: Double, netTotalPrice: Double): Position = {
    this.copy(openTrades = openTrades.enqueue(Purchase(number, netTotalPrice)))
  }

  def sell(number: Double): Position = sell(number, currentPricePerShare)

  def sell(number: Double, pricePerShare: Double): Position = {
    assert(number >= 0, "selling negative number")

    val gain = openTrades.peek(number).map(t => t.shares * pricePerShare - t.netTotalPrice).sum

    val fee = parameters.feeCalculator.byNumberOfShares(number, pricePerShare)

    val closedTrade = ClosedTrade(number, pricePerShare, gain, fee)
    this.copy(openTrades = openTrades.dequeue(number), closedTrades = closedTrade :: closedTrades)
  }
}
